from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator,MaxLengthValidator
# Create your models here.

class User_detail(models.Model):
		first_name = models.CharField(max_length=30,blank=False,null=False,default=None)
		last_name = models.CharField(max_length=30,blank=False,null=False,default=None)
		email = models.CharField(max_length=30,blank=True,unique=True,null=True,default=None)
		mobile = models.CharField(default=None,max_length=11,blank=False,unique=True,null=False,validators=[MinLengthValidator(10,message='Mobile Number sholud be minimum 10 characters'),MaxLengthValidator(11,message='Mobile Number sholud be maximum 11 characters')])
		password = models.CharField(max_length=30,null=False,default=None)
		date_time = models.DateTimeField(auto_now=True,null=False)


