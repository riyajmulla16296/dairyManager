from django import forms
from .models import  User_detail

class userRegistrationForm(forms.ModelForm):
	class Meta:
		model=User_detail
		fields = ['email','first_name','last_name','mobile','password']
		widgets = { 

		'email': forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Email',
			'required': False,
			'type':'email'
			}),
		'first_name': forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'First name',
			'required': True,
			}),			
		'last_name': forms.TextInput(attrs={
			'class':'form-control',
			'name':'firstName',
			'placeholder':'Last name',
			'required': True,
			}),
		'mobile': forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Mobile',
			'required': True,
			}),
		'password': forms.TextInput(attrs={
			'class':'form-control',
			'placeholder':'Password',
			'required': True,
			'type':'password'

			}),

		}
