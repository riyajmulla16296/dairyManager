# Generated by Django 2.1.2 on 2018-11-17 13:48

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_user_detail_password'),
    ]

    operations = [
        migrations.AddField(
            model_name='user_detail',
            name='email',
            field=models.CharField(blank=True, default=None, max_length=30, null=True, unique=True),
        ),
        migrations.AddField(
            model_name='user_detail',
            name='first_name',
            field=models.CharField(default=None, max_length=30),
        ),
        migrations.AddField(
            model_name='user_detail',
            name='last_name',
            field=models.CharField(default=None, max_length=30),
        ),
        migrations.AddField(
            model_name='user_detail',
            name='mobile',
            field=models.CharField(default=None, max_length=11, unique=True, validators=[django.core.validators.MinLengthValidator(10, message='Mobile Number sholud be minimum 10 characters'), django.core.validators.MaxLengthValidator(11, message='Mobile Number sholud be maximum 11 characters')]),
        ),
    ]
